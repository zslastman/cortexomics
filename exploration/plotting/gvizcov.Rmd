---
title: "Pa2g4 - Riboseq coverage"
author: "Dermot Harnett"
date: "`r format(Sys.time(), '%d %B, %Y')`"
output: pdf_document
---
  
  
```{r setup,echo=FALSE,message=FALSE,warning=FALSE}
knitr::opts_chunk$set(root.dir = "~/cortexomics/",echo = FALSE, message = FALSE, warning = FALSE)

library(Gviz)
library(Rsamtools) 
library(rtracklayer)
library(stringr)
library(biomaRt)
library(tidyverse)
library(magrittr)
library(BSgenome.Mmusculus.UCSC.mm10)
select<-dplyr::select
projectfolder <- "~/cortexomics/"
options(ucscChromosomeNames=FALSE)


```


## Plotting Riboseq over Pa2g4
Proof of concept - plotting coverage riboseq over Pa2g4

We use:
- fragments of all sizes
- 

```{r get_annotation,cache=TRUE}
message('loading transcripts')
transcriptfile <- '~/cortexomics/data/static_local/gencode.vM12.annotation.gtf'
if(! exists('transcripts')){ 
  transcripts <- transcriptfile%>% {rtracklayer::import(.)}
  transcripts = transcriptfile %>% import
}
message('getting gene track')
vgene = transcripts%>%subset(gene_name%>%str_detect('Pa2g4') & (type=='gene'))
gchr = vgene%>%seqnames
gstart = (vgene%>%start)
gend = (vgene%>%end)
#create the gene track for gviz

gtrack = transcripts%>%{
    transcripts = .
    transsubset=transcripts%>%subsetByOverlaps(vgene)
    transsubset$feature = transsubset$type %>% as.character
    transsubset<-transsubset[ transsubset$feature %in% c("CDS","UTR")]
    transsubset$exon = transsubset$exon_id
    transsubset$gene = transsubset$gene_name
    transsubset$gene = transsubset$transcript_name
    transsubset$symbol = transsubset$transcript_name
    transsubset$feature = transsubset$feature %>% as.character
    transsubset$transcript= transsubset$transcript_id
    # transsubset$group = transsubset$transcript_id 
    gtrack = Gviz::GeneRegionTrack(transsubset,thinBoxFeature=c("UTR"),showId=TRUE,chromosome=gchr,geneSymbol=TRUE)
}

startchr<-transcripts%>%
  subset(gene_id ==vgene$gene_id)%>%
  subset(type=='start_codon')%>%
  unique

inframewith<-function(gr1,gr2){
  fp2 = ifelse(strand(gr2)=='-',end(gr2),start(gr2))
  fp1 = ifelse(strand(gr1)=='-',end(gr1),start(gr1))
  (fp2 - fp1) %% 3 ==0
}

startcodons<-matchPattern(reverseComplement(DNAString('ATG')),Mmusculus[[gchr%>%as.character]][gstart:gend])%>%
  as('IRanges')%>%
  shift(gstart-1)%>%
  GRanges(gchr,.,strand='-')%>%
  .[inframewith(.,startchr)]

codtrack =     Gviz::AnnotationTrack(startcodons,feature='start_codon',genome='mm10')

```

```{r create_tracks,cache=TRUE}

import.bw.neg <- function(file,selection){
  data <- import.bw(file,sel=selection)
  data$score <- data$score * -1
  data
}

make_track<-function(file,trackname,isneg=FALSE,ylims=NULL...){
  require(Gviz)
  if(str_detect(trackname,'total')) isneg=TRUE
  if(isneg){ 
    importfunction = import.bw.neg
    ylims = c(-20,0)
  }else{
    importfunction= function(file,selection) import.bw(file,sel=selection)
    ylims = c(0,5)
  }
  DataTrack(file,name = trackname,chromosome=gchr,stream=TRUE,importFunction = importfunction,ylim = ylims)
  # DataTrack(file,name = trackname,chromosome=gchr,stream=TRUE,ylim = NULL)
}
datafiles <- Sys.glob(here::here('data/mergedbigwigs/*/*/*'))%>%grep(v=TRUE,inv=TRUE,patt='transcript')
datafiles <- Sys.glob(here::here('data/bigwigs/*/*/*bw'))%>%grep(v=TRUE,inv=TRUE,patt='transcript')
# datafiles <- datafiles[c(1:4)]

bwTracks <- 
  datafiles%>%
  data_frame(file=.)%>%
  mutate(trackname = file%>%basename%>%str_replace('(.transcript)?.bw','') )%>%
  filter(trackname%>%str_detect('ribo.*neg|(total.*pos)'))%>%
  {map2(.$file,.$trackname,.f = make_track)}

# atracks <-map(bam_files[c(1:4,17:20)],.f = .%>%AlignmentsTrack(.,isPaired=FALSE))

# tracks <- c(bwTracks[1:2],gtrack,codtrack,Gviz::GenomeAxisTrack())
# plotTracks(tracks,chr = as.character(gchr), from = gend - 210, to = gend, type="h")
tracks <- c(bwTracks[c(1:4,17:20)],gtrack,codtrack,Gviz::GenomeAxisTrack())
stop()
```


# Pa2g4 - First exon

```{r plot first_exon,fig.height=14,fig.width=10}
pdf(here('exploration','pa2g4_firstexon_bwchr_coverage.pdf'),height=14,w=7)
plotTracks(tracks,chr = as.character(gchr), from = gend - 300, to = gend, type="h")
dev.off()

plotTracks(tracks,chr = as.character(gchr), from = gend - 210, to = gend-180, type="h")


tracks <- c(atracks[1],gtrack,codtrack,Gviz::GenomeAxisTrack())

tracks <- c(AlignmentsTrack(bam_files[1],isPaired=FALSE),gtrack,codtrack,Gviz::GenomeAxisTrack(chromosome=gchr))
tracks <- c(gtrack,codtrack,Gviz::GenomeAxisTrack(chromosome=gchr))

plotTracks(AlignmentsTrack(bam_files[1],isPaired=FALSE,'pileup',chromosome=gchr,genome='mm10'),chr = as.character(gchr), from = gend - 210, to = gend, type="h")

plotTracks(tracks,chr = as.character(gchr), from = gend - 210, to = gend, type="h")

bwTracks[1:2]
tracks <- c(bwTracks[1:2],gtrack,codtrack,Gviz::GenomeAxisTrack())
plotTracks(tracks,chr = as.character(gchr), from = gend - 210, to = gend, type="h")

```


# Pa2g4 - Second exon

```{r plot_second_exon,fig.height=14,fig.width=10}
pdf(here('exploration','pa2g4_secondexon_bwchr_coverage.pdf'),height=14,w=7)
plotTracks(tracks,chr = as.character(gchr), from = gend - 3000, to = gend - 2600, type="h")
dev.off()
```



# Pa2g4 - whole gene

```{r plot_whole_gene,fig.height=14,fig.width=10}

plotTracks(tracks,chr = as.character(gchr), from = gstart - 100, to = gend + 100, type="h")


```
