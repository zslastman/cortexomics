Github wont' let you view html directly, and their markdown system can't handle pdfs. This link using htmlpreview should work though
http://htmlpreview.github.com/?http://github.com/zslastman/cortexomics/master/cortexomics.html

Or, just click on this and save it, then open with your browser  
https://raw.githubusercontent.com/zslastman/cortexomics/master/cortexomics.html
